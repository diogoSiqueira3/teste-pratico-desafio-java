# Teste Pratico - Desafio Java

Aplicação construída para o teste pratico em Java utilizando Maven, SpringBoot e H2.

1 - Para compilar: GIT CLONE no link https://gitlab.com/diogoSiqueira3/teste-pratico-desafio-java.git 
                   Já na IDE (Eclipse) expanda a pasta src/main/java, clique com o botão direito no arquivo "DesafioJavaApplication.java" ou pressione o atalho Alt+Shift+X,J

2 - Para executar o teste unitário: Expanda a pasta src/teste/java, clique com o botão direito no arquivo "DigitoUnicoTest.java" ou pressione o atalho Alt+Shift+X,T

3 - Comentários: 
Conforme combinado com a  Amanda, estou entregando Teste Pratico - Desafio Java dentro do prazo limite (23/12/2020). 

Teste realizado de forma incremental e nesse caso ficou faltando algumas partes que dividirei entre objetivos finalizados e não finalizados (caso haja oportunidade de finaliza-los ficarei muito feliz), sendo eles:

Objetivos finalizados: 
*Digito Unico
*Crud de Usuário
*APIS
*Banco de dados em Memória
*Teste unitario
*Testes integrados com o Postman (collection disponivel na raiz do projeto)

Objetivos não finalizados:
*Criptografia
*Cache
*Endpoint de envio da chave publica
*Arquivo swagger

