package com.testepratico.desafioJava.controller;

import org.junit.Assert;
import org.junit.Test;

import com.testepratico.desafioJava.model.DigitoUnico;

public class DigitoUnicoTest {

	@Test
	public void testCalculaDigitoUnico1p() {
		DigitoUnico digitoUnico = new DigitoUnico();
		
		Assert.assertEquals(2, digitoUnico.calculaDigitoUnico(9875));		
	}
	
	@Test
	public void testCalculaDigitoUnico2p() {
		DigitoUnico digitoUnico = new DigitoUnico();
		
		Assert.assertEquals(9, digitoUnico.calculaDigitoUnico("111", 3));	
	}
}
