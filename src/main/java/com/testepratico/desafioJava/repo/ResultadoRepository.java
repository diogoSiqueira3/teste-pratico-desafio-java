package com.testepratico.desafioJava.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.testepratico.desafioJava.model.Resultado;

@Repository("resultadoRepository")
public interface ResultadoRepository extends JpaRepository<Resultado, Integer>{

}
