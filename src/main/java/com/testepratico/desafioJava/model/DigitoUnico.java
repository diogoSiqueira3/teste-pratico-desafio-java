package com.testepratico.desafioJava.model;

public class DigitoUnico {
	
	public int calculaDigitoUnico(int x) {
		int digito_unico = 0;
		//Verifica se o digito já é unico
		if(x < 10) {
			return x;
		} else {
	        while(x > 0) {
	            digito_unico += (x % 10);
	            x /= 10;
	        }
		
		return calculaDigitoUnico(digito_unico);		
		}
	}
	
	public int calculaDigitoUnico(String n, int k) {				
		String s = new String(); 
		
 		for (int i = 0; i < k; i++) {
			s += n;
		}
 		
		return calculaDigitoUnico(Integer.parseInt(s));
	}
}

