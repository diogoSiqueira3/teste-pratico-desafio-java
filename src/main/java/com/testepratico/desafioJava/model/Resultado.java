package com.testepratico.desafioJava.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Resultado {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	//Parametro de entrada inteiro
	private int pEntradaInt;
	
	//Parametro de entrada string (numero de vezes da concatenação)
	private String pEntradaString;	
	
	private int resultado;
	
	@ManyToOne
	@JoinColumn(name="usuario_id", nullable=true)
	private Usuario usuario;
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public int getpEntradaInt() {
		return pEntradaInt;
	}

	public String getpEntradaString() {
		return pEntradaString;
	}

	public void setpEntradaString(String pEntradaString) {
		this.pEntradaString = pEntradaString;
	}

	public void setpEntradaInt(int pEntradaInt) {
		this.pEntradaInt = pEntradaInt;
	}
	
	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	
}
