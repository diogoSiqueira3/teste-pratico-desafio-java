package com.testepratico.desafioJava.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.testepratico.desafioJava.model.DigitoUnico;
import com.testepratico.desafioJava.model.Resultado;
import com.testepratico.desafioJava.model.Usuario;
import com.testepratico.desafioJava.service.ResultadoService;

@RestController
public class ResultadoApiController {
	@Autowired
	private ResultadoService ResultadoService;	
	
	@RequestMapping(value="/api/CalculaDigitoUnico", method = RequestMethod.GET)
	public Resultado retornarResultadoDigitoUnico(@RequestParam(value="inteiro") String inteiro, 
												  @RequestParam(value="concat", required=false) String concat,
												  @RequestBody(required=false) Usuario usuario) {
		
		DigitoUnico digitoUnico = new DigitoUnico();
		Resultado resultado = new Resultado();	
		
		if(concat == null) {
			resultado.setpEntradaInt(Integer.parseInt(inteiro));
			resultado.setResultado(digitoUnico.calculaDigitoUnico(Integer.parseInt(inteiro)));
		} else {
			resultado.setpEntradaInt(Integer.parseInt(inteiro));
			resultado.setpEntradaString(concat);
			resultado.setResultado(digitoUnico.calculaDigitoUnico(inteiro, Integer.parseInt(concat)));
		}		
		
		if(usuario != null) {
			resultado.setUsuario(usuario);
		}
		
		ResultadoService.insertResultado(resultado);		
		
		return resultado;
	}	
}