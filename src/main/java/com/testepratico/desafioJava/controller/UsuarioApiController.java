package com.testepratico.desafioJava.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.testepratico.desafioJava.model.Resultado;
import com.testepratico.desafioJava.model.Usuario;
import com.testepratico.desafioJava.service.UsuarioService;

@RestController
public class UsuarioApiController {
	@Autowired
	private UsuarioService usuarioService;	
	
	@RequestMapping(value = "/api/Usuario", method = RequestMethod.GET)
	public List<Usuario> retornarListaUsuarios(){
		return usuarioService.getAllUsuarios();
	}
	
	@RequestMapping(value="/api/Usuario/{id}", method = RequestMethod.GET)
	public Optional<Usuario> retornarUsuario(@PathVariable("id") Integer id) {
		return usuarioService.getUsuarioById(id);
	}
	
	@RequestMapping(value="/api/Usuario/{id}/Resultados", method = RequestMethod.GET)
	public List<Resultado> retornarResultadosUsuario(@PathVariable("id") Integer id) {
	
		return usuarioService.getUsuarioById(id).get().getResultados();
	}
	
	@RequestMapping(value = "/api/Usuario/Inserir", method = RequestMethod.POST)
    public void incluirUsuario(@RequestBody Usuario usuario) {
	 usuarioService.insertUsuario(usuario);
	}	
	
	@RequestMapping(value = "/api/Usuario/Alterar/{id}", method = RequestMethod.POST)
	    public void alterarUsuario(@RequestBody Usuario usuario, @PathVariable("id") Integer id) {
		 usuarioService.updateUsuarioById(id, usuario);
	}
	 
	@RequestMapping(value = "/api/Usuario/Apagar/{id}", method = RequestMethod.DELETE)
	    public void apagarUsuario(@PathVariable("id") Integer id) {
		 usuarioService.deleteUsuarioById(id);
	 } 
}
