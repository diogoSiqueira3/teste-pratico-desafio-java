package com.testepratico.desafioJava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.testepratico.desafioJava.model.Resultado;
import com.testepratico.desafioJava.repo.ResultadoRepository;

@Service("veiculoService")
public class ResultadoServiceImpl implements ResultadoService {
	@Autowired
	ResultadoRepository resultadoRepository;
	
	@Override
	public Optional<Resultado> getResultadoById(Integer id) {
		return resultadoRepository.findById(id);
	}

	@Override
	public List<Resultado> getAllResultados() {
		return resultadoRepository.findAll();
	}

	@Override
	public void deleteAllResultados() {
		resultadoRepository.deleteAll();
	}

	@Override
	public void deleteResultadoById(Integer id) {
		resultadoRepository.deleteById(id);		
	}

	@Override
	public void insertResultado(Resultado resultado) {
		resultadoRepository.save(resultado);		
	}
}
