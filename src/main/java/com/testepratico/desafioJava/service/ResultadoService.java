package com.testepratico.desafioJava.service;

import java.util.List;
import java.util.Optional;

import com.testepratico.desafioJava.model.Resultado;

public interface ResultadoService {
	Optional<Resultado> getResultadoById(Integer id);
	List<Resultado> getAllResultados();
	void deleteAllResultados();
	void deleteResultadoById(Integer id);
	void insertResultado(Resultado Resultado);
}
